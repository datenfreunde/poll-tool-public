var H5P = H5P || {};
var pollObject = {};

H5P.dataHUB = (function ($) {
    /**
     * Constructor function.
     */
    function C(options, id) {
        // Extend defaults with provided options
        this.options = $.extend(true, {}, {
            question: null,
            image: null
        }, options);
        // Keep provided id.
        this.id = id;
    }

    var content = { //TODO: add translations
        "resultView": {
            "disclaimer": {
                "en": "This is a sample Disclaimer",
                "de": "Dies ist ein Beispiel-Disclaimer"
            },
            "count": {
                "en": " Votes",
                "de": " Stimmen",
                "fr": " Voix",
                "es": " Votos",
                "it": " Voti"
            }
        }
    };

    /**
     * Attach function called by H5P framework to insert H5P content into
     * page
     *
     * @param {jQuery} $container
     */

    C.prototype.attach = function ($container) {

        /*console.log("THIS", this);
        let that = this;
        function sendPoll(that) {
            let pollObject = {};

            that.options.pollID
        }*/





        /**
         *
         * Get Country via IP Address
         *
         **/
        var userCountry = '';
        var query = window.location.search.substring(1).split("&");

        /*$.ajax({
            url: "https://ipinfo.io",
            type: "GET",
            dataType: "json",
            async: false,
            success: function (data) {
                //console.log(data);
                userCountry = data.country.toLowerCase();
            }
        });*/

        userCountry = $('html')[0].lang.slice(0,2);

        for (var i=0;i<query.length;i++) {
            if(query[i].split("=")[0] == "lang" && query[i].split("=")[1] != "") {
                userCountry = query[i].split("=")[1];
            }
        }
        /**
         *
         * Get all PollIDs
         *
         **/


        let $wrapper = $('<div>', {
            'class': 'wrapper'
        });
        $wrapper.addClass('animation');

        let $pollView = $('<div>', {

            'class': 'pollView'
        });

        let $resultView = $('<div>', {

            'class': 'resultView'
        });

        /*let $availableLanguages = $('<div>', {
            'class': 'languageWrapper'
        });

        $pollView.append($availableLanguages);

        console.log(this.options.questionList);*/

        $wrapper.append($pollView);
        $wrapper.append($resultView);



        $container.addClass("h5p-dataHUB-container");

        var availableLanguages = [];
        //sorry, realised too late that i need this
        this.options.questionList.forEach(function (pollItem) {
            availableLanguages.push(pollItem.pollLanguage);
        });

        var preID = this.options.pollID;

        let $totalCount = $('<div>', {
            'id': 'totalCount' + preID,
            'class': 'totalCount inactive'
        });

        this.options.questionList.forEach(function (pollItem) {

            let language = pollItem.pollLanguage;
            let pollID = preID + '_' + language;


            switch (userCountry) {
                case 'de':
                    break;
                case 'fr':
                    break;
                case 'es':
                    break;
                case 'it':
                    break;
                case 'en':
                    break;
                case '':
                    userCountry = 'en';
                    console.log("Language not found in <html>-Tag, using default (en) or first available Language");
                    break;
                default:
                    userCountry = 'en';
                    console.log("Language not found in <html>-Tag, using default (en) or first available Language");
                    break;
            }

            if (!availableLanguages.includes(userCountry)) {

                userCountry = availableLanguages[0];
            }

            if (language == userCountry) {

                let $pollWrapper = $('<div>', {
                    'id': pollID,
                    'class': 'active'
                });

                addQuestion(pollID, $pollWrapper, pollItem, $pollView/*, $resultView*/)


            } else {

                let $pollWrapper = $('<div>', {
                    'id': pollID,
                    'class': 'inactive'
                });

                addQuestion(pollID, $pollWrapper, pollItem, $pollView/*, $resultView*/)
            }

            //createFlags(language, pollID);

            $resultView.append($totalCount);

            $container.append($wrapper);

            //console.log($pollView.height());
            $container.height(($pollView.height() + 40));

            //console.log("OPTIONS: ", this.options);
        });


        function addQuestion(pollID, $pollWrapper, pollItem, $pollView/*, $resultView*/) {
            $pollView.append($pollWrapper);

            let question = document.createElement("DIV");
            question.setAttribute("data-pollID", pollID);
            question.setAttribute("class", "question");
            question.innerHTML = pollItem.question;

            $pollWrapper.append(question);

            let $answerWrapper = $('<div>', {
                'id': 'result' + pollID,
                'class': 'resultWrapper inactive'
            });

            $answerWrapper.append(question.cloneNode(true));

            pollItem.answers.forEach(function (answer, index) {

                pollObject = {
                    question: pollItem.question,
                    answers: {}
                };

                pollObject.answers[index] = answer;

                let $button = $('<button>', {
                    'class': 'answer',
                    'role': 'button',
                    'html': answer,
                    'data-pollID': pollID,
                    on: {
                        click: function sendObject() {

                            $wrapper.toggleClass('animation-active');

                            if (typeof ga === 'undefined') {

                                (function (i, s, o, g, r, a, m) {
                                    i['GoogleAnalyticsObject'] = r;
                                    i[r] = i[r] || function () {
                                            (i[r].q = i[r].q || []).push(arguments)
                                        }, i[r].l = 1 * new Date();
                                    a = s.createElement(o),
                                        m = s.getElementsByTagName(o)[0];
                                    a.async = 1;
                                    a.src = g;
                                    m.parentNode.insertBefore(a, m)
                                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                                ga('create', 'UA-98436101-1', 'auto');

                                console.log("ga created");

                                ga('send', {
                                    hitType: 'event',
                                    eventCategory: 'Survey',
                                    eventAction: pollID,
                                    eventLabel: index

                                });

                                let givenAnswer = pollID.toString() + index; //selector for queryReports() -> displayResults()
                                console.log("event sent ");

                                queryReports(pollID, givenAnswer);

                            } else {

                                ga('send', {
                                    hitType: 'event',
                                    eventCategory: 'Survey',
                                    eventAction: pollID,
                                    eventLabel: index
                                });

                                let givenAnswer = pollID.toString() + index; //selector for queryReports() -> displayResults()
                                console.log("event sent ");

                                queryReports(pollID, givenAnswer);
                            }


                        }
                    }
                });


                let $answer = $('<div>', {
                    'class': 'resultAnswer',
                    'html': answer
                });

                let $bar = $('<div>', {
                    'id': pollID.toString() + index,
                    'class': 'resultBar'
                });

                let $label = $('<label>', {
                    'id': 'answerLabel' + pollID + index,
                    'class': 'label',
                    'html': '0%'
                });


                $answerWrapper.append($answer);
                $answerWrapper.append($bar);
                $answerWrapper.append($label);

                $pollWrapper.append($button);
            });

            $resultView.append($answerWrapper);

        }

        function createFlags(language, pollID) {

            //console.log(pollID);

            let $flag = $('<img>', {
                'src': 'wp-content/uploads/2017/06/' + language.toUpperCase() + '.png',
                'class': 'flag',

                'on': {
                    click: function changeLanguage() {
                        let selector = '#' + pollID + '_' + language;
                        //console.log(this.parentNode.parentNode.children[pollID]);
                        let pollCollection = this.parentNode.parentNode.children;

                        userCountry = language;

                        for (var [entry, value] of Object.entries(pollCollection)) {
                            if (entry == pollID) {
                                //console.log("checkEntries ", pollCollection[entry].attributes[1].nodeValue);
                                if (pollCollection[entry].attributes[1].nodeValue == 'inactive') {
                                    pollCollection[entry].attributes[1].nodeValue = 'active'
                                }
                            } else {
                                if (pollCollection[entry].attributes[1].nodeValue == 'active') {
                                    pollCollection[entry].attributes[1].nodeValue = 'inactive'
                                }
                            }
                        }
                    }
                }
            });

            //$availableLanguages.append($flag);
        }

        function queryReports(dataID, givenAnswer) {

            $.ajax({
                url: "https://s3.eu-central-1.amazonaws.com/datahub-poll-data/" + dataID + ".json",
                type: "GET",
                dataType: "json",
                async: false,
                success: function (response) {
                    displayResults(response, dataID, givenAnswer);
                    $resultView.addClass('loaded');
                },
                error: function() {
                    showFirstVote(dataID, givenAnswer);
                    $resultView.addClass('loaded');
                }
            });
        }

        function displayResults(response, pollID, givenAnswer) {

            let resultSelector = '#result' + pollID;

            $(resultSelector).toggleClass('inactive active');

            var surveyObject = response;
            //console.log(response);


                /**
                 * Total count of answers
                 **/
                let total = Object.values(surveyObject).reduce((a, b) => a + b); //sum up all Values
                total += 1;
                let countSelector = '#totalCount' + preID;
                $(countSelector).html(total + content.resultView.count[userCountry]);
                $(countSelector).toggleClass('inactive active');

                let answer = givenAnswer.slice(-1);
                if(answer in surveyObject) {
                    surveyObject[answer] += 1;
                } else {
                    surveyObject[answer] = 1;
                }

                for (var element in surveyObject) {

                    let barSelector = '#' + pollID + element;
                    let labelSelector = '#answerLabel' + pollID + element;

                    let ratio = (surveyObject[element] / total); //148px from Styling (wrappersize - 2*labels - padding)

                    $(barSelector).css('width', ratio * 465 + 'px');
                    $(labelSelector).html((ratio * 100).toFixed(1) + '%');
                }


        }

        function showFirstVote(pollID, givenAnswer) {
            let resultSelector = '#result' + pollID;

            $(resultSelector).toggleClass('inactive active');

            let total = '1';
            let countSelector = '#totalCount' + preID;
            $(countSelector).html(total + content.resultView.count[userCountry]);

            $(countSelector).toggleClass('inactive active');

            let barSelector = '#' + givenAnswer;
            let labelSelector = '#answerLabel' + givenAnswer;

            //console.log(barSelector, labelSelector);

            $(barSelector).css('width', 220 + 'px');
            $(labelSelector).html((100).toFixed(1) + '%');


        }

        function overrideANSA()

    };

    return C;

})(H5P.jQuery);