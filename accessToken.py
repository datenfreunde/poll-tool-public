from oauth2client.service_account import ServiceAccountCredentials

# The scope for the OAuth2 request.
SCOPE = 'https://www.googleapis.com/auth/analytics.readonly'

# The location of the key file with the key data.
KEY_FILEPATH = 'dataHUB_service.json'

# Defines a method to get an access token from the ServiceAccount object.
def get_access_token():
	print ServiceAccountCredentials.from_json_keyfile_name(KEY_FILEPATH, SCOPE).get_access_token().access_token
	return ServiceAccountCredentials.from_json_keyfile_name(KEY_FILEPATH, SCOPE).get_access_token().access_token

get_access_token()

# ya29.EllrBHQRFzah7_xdmG0KcnMdiJanJQziHWZThwKOUIkup1TH3ZiRfQjKzgXHpftay6cxgPQXeYpG97kM8Zzsrf0wp6zOGWYQx5MW8Vjh2FV46e8twKjqjvDsmA