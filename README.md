## What does this Project do?

Simple JS wrapped as H5P Content-Type for sending/getting Data to/from G-Analytics (actually there is a bridge on an aws s3 bucket communicating with G-Analytics)

## How to start developing


Docker:
For local development you can use a script that sets up wordpress in a docker container.
Requires docker and docker-compose. Run the script for quick setup:

    ./local-docker-wordpress-setup.sh


- Setup environment (e.g. Wordpress (see docker above), Drupal, ...)
- install H5P (e.g. for Wordpress)
- develop a new Content-Type and Upload it (e.g. to Wordpress, read more in "Deploy library")

For more details see Documentation
https://h5p.org/documentation


## Deploy library (Wordpress)

First, for any update to take effect when updating an existing library, you will need to make sure the new version is higher than the version on the current library.
The current version used can be found on wordpress Dashboard -> H5P Content -> Libraries.

To manually update the version, edit the appropriate version property in:

`/H5P.dataHUB/library.json`

Then create the h5p file for upload:

    ./h5p.sh

But in order for changes to take effect, you will need to manually update the `patch` version in

`/H5P.dataHUB/library.json`

Once you're done, go to the wordpress admin dashboard -> H5P Content -> Libraries. Here you select your .h5p file for upload under "Upload Libraries" and check "Only update existing libraries".

## How to see the poll results in Google Analytics

There ist a prepared Dashboard where you can see how polls are answered. It's part of the EDNH project. The dashboard is explained here:
https://docs.google.com/document/d/1FTk2QIpsbQ1wk-Hi3MtGTwY3Eh1m8h4c980E1QAnhto/edit#

The dashboard can be accessed at:

https://analytics.google.com/analytics/web/?authuser=1#dashboard/a_AqIbcsQsG9rDSk5cFYeA/a98436101w144813927p149582099/%3F_u.date00%3D20171001%26_u.date01%3D20171210%26_.useg%3Dbuiltin1/ (Freischaltung auf Anfgrage). 

Feel free to ask kn@datenfreunde.com, cz@datenfreunde.com, jh@datenfreunde.com, mv@datenfreunde.com or sebastian.zilles@dpa-info.com for access.

## This was used to setup the bridge for accessing G-Analytics-Data 

$ pip install --upgrade google-api-python-client

run 'python accessToken.py'

