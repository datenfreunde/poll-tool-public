#!/bin/bash

set -e

sed '4q;d' H5P.dataHUB/library.json
sed '5q;d' H5P.dataHUB/library.json
sed '6q;d' H5P.dataHUB/library.json

rm H5P.dataHUB.h5p
zip -r H5P.dataHUB.zip H5P.dataHUB
mv H5P.dataHUB.zip H5P.dataHUB.h5p