var H5P = H5P || {};
var pollObject = {};

H5P.dataHUB = (function ($) {
    /**
     * Constructor function.
     */
    function C(options, id) {

        // Extend defaults with provided options
        this.options = $.extend(true, {}, {
            question: null,
            image: null
        }, options);
        // Keep provided id.
        this.id = id;
    }

    var content = {
        "resultView": {
            "disclaimer": {
                "en": "This is a sample Disclaimer",
                "de": "Dies ist ein Beispiel-Disclaimer"
            },
            "count": {
                "en": " Votes",
                "de": " Stimmen",
                "fr": " Voix",
                "es": " Votos",
                "it": " Voti"
            }
        },
        "stateView": {
            "closed": {
                "en": "This poll is closed.",
                "de": "Diese Umfrage ist geschlossen.",
                "fr": "Ce sondage est fermé",
                "es": "Esta encuesta está cerrada",
                "it": "Questo sondaggio è chiuso"
            }

        }
    };

    /**
     * Attach function called by H5P framework to insert H5P content into
     * page
     *
     * @param {jQuery} $container
     */

    C.prototype.attach = function ($container) {


        /**
         *
         * Get Country via IP Address
         *
         **/
        var userCountry = '';
        var query = window.location.search.substring(1).split("&");

        userCountry = $('html')[0].lang.slice(0,2);

        for (var i=0;i<query.length;i++) {
            if(query[i].split("=")[0] == "lang" && query[i].split("=")[1] != "") {
                userCountry = query[i].split("=")[1];
            }
        }
        /**
         *
         * Get all PollIDs
         *
         **/


        let $wrapper = $('<div>', {
            'class': 'wrapper'
        });
        $wrapper.addClass('animation');

        let $pollView = $('<div>', {

            'class': 'pollView'
        });

        let $resultView = $('<div>', {

            'class': 'resultView'
        });

        $container.addClass("h5p-dataHUB-container");

        var availableLanguages = [];
        //sorry, realised too late that i need this
        this.options.questionList.forEach(function (pollItem) {
            availableLanguages.push(pollItem.pollLanguage);
        });

        var preID = this.options.pollID;

        let $totalCount = $('<div>', {
            'id': 'totalCount' + preID,
            'class': 'totalCount inactive'
        });

        switch (userCountry) {
            case 'de':
                break;
            case 'fr':
                break;
            case 'es':
                break;
            case 'it':
                break;
            case 'en':
                break;
            case '':
                userCountry = 'en';
                console.log("Language not found in <html>-Tag, using default (en) or first available Language");
                break;
            default:
                userCountry = 'en';
                console.log("Language not found in <html>-Tag, using default (en) or first available Language");
                break;
        }

        var languageFound = false;

        for (var i = 0; i < availableLanguages.length; i++) {

            console.log(i, userCountry, availableLanguages[i]);
            if (userCountry == availableLanguages[i]) {
                userCountry = availableLanguages[i];
                languageFound = true;
                console.log(availableLanguages[i], 'found');
                break;
            }
        }

        if(languageFound == false) {
            userCountry = availableLanguages[0];
            console.log("This poll is not available in your language. Language is set to ", availableLanguages[0]);
        }

        let pollID = preID + '_' + userCountry;
        console.log("LANG: ", userCountry);

        let pollState = this.options.pollState;

        let $stateView = $('<div>', {
            'class': 'closed'
        });

        let $stateText = $('<div>', {
            'class': 'overlayText',
            'html': content.stateView.closed[userCountry]
        });

        if (this.options.pollState == 'active') {

            $wrapper.append($pollView);
            $wrapper.append($resultView);

        } else {

            if(this.options.pollState == 'showResults') {
                $pollView.css("display", "none");
                $wrapper.css("width", "100%");
            }

            $wrapper.append($pollView);
            $wrapper.append($resultView);

            $stateView.append($stateText);
            $container.append($stateView);

        }

        let voteCountThreshold = this.options.voteCountThreshold;

        this.options.questionList.forEach(function (pollItem) {

            let language = pollItem.pollLanguage;
            let pollID = preID + '_' + language;

            if (language == userCountry) {

                let $pollWrapper = $('<div>', {
                    'id': pollID,
                    'class': 'active'
                });

                addQuestion(pollID, $pollWrapper, pollItem, $pollView, voteCountThreshold/*, $resultView*/)


            } else {

                let $pollWrapper = $('<div>', {
                    'id': pollID,
                    'class': 'inactive'
                });

                addQuestion(pollID, $pollWrapper, pollItem, $pollView, voteCountThreshold/*, $resultView*/)
            }

            //createFlags(language, pollID);

            $resultView.append($totalCount);

            $container.append($wrapper);

            //console.log($pollView.height());

            $container.height(($pollView.height() + 60));



        });

        if(pollState == 'showResults') {
            console.log("QUERYID: ",pollID);

            queryReports(pollID, voteCountThreshold);

            let selector = "#result" + pollID;
            $(selector).toggleClass("inactive active");
            //$wrapper.toggleClass("animation-active");
        }

        function addQuestion(pollID, $pollWrapper, pollItem, $pollView, $voteCountThreshold/*, $resultView*/) {
            if (pollID.split('_')[0] !== 'undefined' && pollID !== '') {
                console.log("IF", pollID, typeof pollID);
                $pollView.append($pollWrapper);

                let question = document.createElement("DIV");
                question.setAttribute("data-pollID", pollID);
                question.setAttribute("class", "question");
                question.innerHTML = pollItem.question;

                $pollWrapper.append(question);

                let $answerWrapper = $('<div>', {
                    'id': 'result' + pollID,
                    'class': 'resultWrapper inactive'
                });

                $answerWrapper.append(question.cloneNode(true));

                pollItem.answers.forEach(function (answer, index) {

                    pollObject = {
                        question: pollItem.question,
                        answers: {}
                    };

                    pollObject.answers[index] = answer;

                    let $button = $('<button>', {
                        'class': 'answer',
                        'role': 'button',
                        'html': answer,
                        'data-pollID': pollID,
                        on: {
                            click: function sendObject() {

                                if (pollState == 'active') {

                                    $wrapper.toggleClass('animation-active');


                                    if (typeof ga === 'undefined') {

                                        (function (i, s, o, g, r, a, m) {
                                            i['GoogleAnalyticsObject'] = r;
                                            i[r] = i[r] || function () {
                                                    (i[r].q = i[r].q || []).push(arguments)
                                                }, i[r].l = 1 * new Date();
                                            a = s.createElement(o),
                                                m = s.getElementsByTagName(o)[0];
                                            a.async = 1;
                                            a.src = g;
                                            m.parentNode.insertBefore(a, m)
                                        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                                        ga('create', 'UA-98436101-1', 'auto');

                                        console.log("ga created");

                                        if (pollID && !isNaN(parseInt(index))) {

                                            ga('send', {
                                                hitType: 'event',
                                                eventCategory: 'Survey',
                                                eventAction: pollID,
                                                eventLabel: index

                                            });
                                        }

                                        let givenAnswer = pollID.toString() + index; //selector for queryReports() -> displayResults()
                                        console.log("event sent ");
                                        console.log("Q2 ", pollID);
                                        queryReports(pollID, $voteCountThreshold, givenAnswer);

                                    } else {

                                        if (pollID && !isNaN(parseInt(index))) {
                                            
                                            ga('send', {
                                                hitType: 'event',
                                                eventCategory: 'Survey',
                                                eventAction: pollID,
                                                eventLabel: index
                                            });
                                        }

                                        let givenAnswer = pollID.toString() + index; //selector for queryReports() -> displayResults()
                                        console.log("event sent ");

                                        queryReports(pollID, $voteCountThreshold, givenAnswer);
                                    }

                                }
                            }
                        }
                    });


                    let $answer = $('<div>', {
                        'class': 'resultAnswer',
                        'html': answer
                    });

                    let $bar = $('<div>', {
                        'id': pollID.toString() + index,
                        'class': 'resultBar'
                    });

                    let $label = $('<label>', {
                        'id': 'answerLabel' + pollID + index,
                        'class': 'label',
                        'html': '0%'
                    });


                    $answerWrapper.append($answer);
                    $answerWrapper.append($bar);
                    $answerWrapper.append($label);

                    $pollWrapper.append($button);

                });
                $resultView.append($answerWrapper);

            } else {
                console.log("ELSE");
                let $answerWrapper = $('<div>', {
                    'id': 'empty',
                    'class': 'resultWrapper inactive',
                    'html': 'POLL-ID missing!'
                });
                $pollView.html("POLL ID MISSING!").css('font-size', '2rem');
            }


        }

        function queryReports(dataID, voteCountThreshold, givenAnswer) {

            $.ajax({
                url: "https://s3.eu-central-1.amazonaws.com/datahub-poll-data/" + dataID + ".json",
                type: "GET",
                dataType: "json",
                async: false,
                success: function (response) {
                    displayResults(response, dataID, givenAnswer, voteCountThreshold);
                    $resultView.addClass('loaded');
                },
                error: function() {
                    showFirstVote(dataID, givenAnswer, voteCountThreshold);
                    $resultView.addClass('loaded');
                }
            });
        }

        function displayResults(response, pollID, givenAnswer, voteCountThreshold) {


            console.log("POLL ",pollID);
            let resultSelector = '#result' + pollID;

            if(pollState == 'active') {
                $(resultSelector).toggleClass('inactive active');

            }

            var surveyObject = response;
            //console.log(pollID, response);


            /**
             * Total count of answers
             **/
//            let total = Object.values(surveyObject).reduce(function(a, b) { return a + b }); //sum up all Values

            let values = Object.keys(surveyObject).map(function(e) {
                return surveyObject[e]
            });

            let total = values.reduce(function(a,b) { return a+b });

            if(givenAnswer != undefined) {

                total += 1;

                let answer = givenAnswer.slice(-1);
                if (answer in surveyObject) {
                    surveyObject[answer] += 1;
                } else {
                    surveyObject[answer] = 1;
                }

            }

            let countSelector = '#totalCount' + preID;

            let label = _getTotalVotesLabel(total, userCountry, voteCountThreshold);
            $(countSelector).html(label);
            $(countSelector).toggleClass('inactive active');



            for (var element in surveyObject) {
                if(element != '(not set)') {
                    let barSelector = '#' + pollID + element;
                    let labelSelector = '#answerLabel' + pollID + element;

                    let ratio = (surveyObject[element] / total); //148px from Styling (wrappersize - 2*labels - padding)

                    $(barSelector).css('width', ratio * 85 + '%');

                    if(parseInt(total) < 1000) {
                        $(labelSelector).html((ratio * 100).toFixed(1) + '%');
                    } else {
                        $(labelSelector).html((ratio * 100).toFixed(1) + '% (' + surveyObject[element] + ' ' + content.resultView.count[userCountry] + ')');
                    }

                    console.log("BAR: ", barSelector, "LABEL: ", labelSelector, "RATIO: ", ratio)
                }
            }


        }

        function showFirstVote(pollID, givenAnswer, voteCountThreshold) {

            let resultSelector = '#result' + pollID;

            $(resultSelector).toggleClass('inactive active');

            let label = _getTotalVotesLabel(1, userCountry, voteCountThreshold);
            let countSelector = '#totalCount' + preID;
            $(countSelector).html(label);

            $(countSelector).toggleClass('inactive active');

            let barSelector = '#' + givenAnswer;
            let labelSelector = '#answerLabel' + givenAnswer;

            //console.log(barSelector, labelSelector);

            $(barSelector).css('width', 220 + 'px');
            $(labelSelector).html((100).toFixed(1) + '%');


        }

        if(this.options.cssOverride == "ANSA") {
            $('.answer').css("height", "35px");
            $('.question').css("font-size", "1rem");
            $container.height(($resultView.height() + 60 ));
            $container.css("overflow", "hidden");
            console.log("ANSA");
        }

        function _getTotalVotesLabel (total, userCountry, voteCountThreshold) {

            let label = total + content.resultView.count[userCountry];
            let threshold = voteCountThreshold | 0;
            if (total < threshold) {
                label = "";
            }
            return label;
        }

    };

    return C;

})(H5P.jQuery);